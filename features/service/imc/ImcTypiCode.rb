require_relative  '../../support/company_log.rb'

require 'httparty'
#require 'openssl' ##2018Jun14 - quero deixar de usar openssl, dificil de instalar!

require 'json'

class InmetricsTypiCode

	attr_accessor :status
	attr_accessor :error
	attr_accessor :response_body
	attr_accessor :response_body_hash_array

	def initialize(id=nil, title=nil, due_datetime=nil, completed=nil)
		@status = nil
		@response_body = nil
		@response_body_hash_array = nil
		@rerror = nil
	end

	def call
		status = nil
		options = {
			headers: { 
				"Accept" => "application/json"
			}
		}
		
		status = nil
		response = nil
		response_body = nil
		error = nil
		begin
			response = HTTParty.get("https://jsonplaceholder.typicode.com/todos", options)
			write_company_log "response.inspect=#{response.inspect}"
			response_body = response.body
			status = response.code
		rescue HTTParty::Error => httparty_error
			write_company_log "httparty_error.class=#{httparty_error.class}"
			error = httparty_error
		rescue SystemCallError => system_call_error
			write_company_log "system_call_error.class=#{system_call_error.class}"
			error = system_call_error
		end
		@status = status
		@response_body = response_body
		begin
			@response_body_hash_array = JSON.parse(response_body)
		rescue Exception => e
			@response_body_hash_array = {"response_body_parsing_error":true}
			#faz nada
		end
		@error = error
	
		if @error
			falhar "Exception/error #{@error} when calling GET"
		end
	end


	def check_status_ok
		if @status != 200
			falhar "Status code (@status=#{@status}) not ok, values_descr=#{values_descr}"
		end
	end

	def check_has_complete_true
		mismatches = []

		if not response_body_hash_array.is_a? Array
			falhar "bad response json, one of the rows lacks 'completed, boolean'"
		end 
		any_completed = false
		response_body_hash_array.each do |item|

			if not [true,false].include? item['completed']
				falhar "bad response json, one of the rows lacks 'completed, boolean', item=#{item}"
			end 
			if item['completed']
				if not any_completed 
					any_completed = true
				end
				write_company_log "RETURNED , complted item=#{item}"
				puts "completed item=#{item}" #nao apareceu em output reports.html, TODO - corrigir!
			end
		end
		if not any_completed
			falhar "No completed item found"
		end
	end


end #end class


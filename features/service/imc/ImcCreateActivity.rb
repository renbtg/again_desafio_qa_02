require_relative  '../../support/company_log.rb'

require 'httparty'
#require 'openssl' ##2018Jun14 - quero deixar de usar openssl, dificil de instalar!

require 'json'

class InmetricsCreateActivity
	attr_accessor :id
	attr_accessor :title
	attr_accessor :due_datetime

	attr_accessor :completed
	attr_accessor :status
	attr_accessor :error
	attr_accessor :response_body
	attr_accessor :response_body_hash

	def initialize(id=nil, title=nil, due_datetime=nil, completed=nil)
		@id = id || $massa['ID'].to_i
		@title = title || $massa['TITLE']
		@due_datetime = time_millis4(Time.parse((due_datetime||$massa['DUE_DATE']).to_s))
		@completed = completed || (not ['false','0','N'].include?($massa['COMPLETED'].downcase)) #false, 0 e N are $massa values for boolean=false

		@status = nil
		@response_body = nil
		@response_body_hash = nil
		@rerror = nil
	end

	def call
		status = nil
		options = {
			body: {
				ID: @id,
				Title: @title,
				DueDate: time_to_str(@due_datetime),#14964822125
				Completed: "#{@completed}"
			}.to_json,
			
			headers: { 
				"Content-Type" => "application/json",
				"Accept" => "application/json"
			}
		}
		
		status = nil
		response = nil
		response_body = nil
		error = nil
		begin
			response = HTTParty.post("http://fakerestapi.azurewebsites.net/api/Activities/#{@id}", options)
			write_company_log "response.inspect=#{response.inspect}"
			response_body = response.body
			status = response.code
		rescue HTTParty::Error => httparty_error
			write_company_log "httparty_error.class=#{httparty_error.class}"
			error = httparty_error
		rescue SystemCallError => system_call_error
			write_company_log "system_call_error.class=#{system_call_error.class}"
			error = system_call_error
		end
		@status = status
		@response_body = response_body
		begin
			@response_body_hash = JSON.parse(response_body)
		rescue Exception => e
			@response_body_hash = {"response_body_parsing_error":true}
			#faz nada
		end
		@error = error
	
		if @error
			falhar "Exception/error #{@error} when calling POST"
		end
	end


	def check_status_ok
		if @status != 200
			falhar "Status code (@status=#{@status}) not ok, values_descr=#{values_descr}"
		end
	end

	def check_request_response_matches
		mismatches = []
		if response_body_hash['ID'].to_s != id.to_s
			mismatches << 'ID'
		end
		if response_body_hash['Completed'].to_s != completed.to_s 
			mismatches << 'Completed'
		end
		if response_body_hash['Title'].to_s != title.to_s 
			mismatches << 'Title'
		end
		if response_body_hash['DueDate'] != time_to_str(due_datetime)
			write_company_log :error, "DueDate mistach, #{response_body_hash['DueDate']}, #{time_to_str(due_datetime)}"
			mismatches << 'DueDate'
		end
		
		if mismatches.length == 0
			write_company_log "OK check_request_response_matches, values_descr=#{values_descr}"
		else
			falhar "check_request_response_matches FAILED, mismatches=#{mismatches}, values_descr=#{values_descr}"
		end
	end

private
	def values_descr
		"@id=#{@id}, @title=#{@title}, @due_datetime=#{@due_datetime}, @completed=#{@completed}, @response_body_hash=#{@response_body_hash}, @response_body=#{@response_body}, @status=#{@status}, @error=#{@error}"
	end

	def time_to_str(time)
		if time.nsec != 0
			time.strftime('%Y-%m-%dT%H:%M:%S.%4N')
		else
			time.strftime('%Y-%m-%dT%H:%M:%S')
		end
	end

	def time_millis4(time)
		Time.mktime(time.year, time.month, time.day, time.hour, time.min, time.sec, time.nsec/100000)  
	end

	def str_to_time(str)
		str = str.to_s #just in case we get nil, generalization

		#012345678900123456789012
		#2018-10-23T12:34:56.1234
		year = str[0..3]
		month = str[5..6]
		day = str[8..9]
		hour = str[11..12]
		minutes = str[14..15]
		seconds = str[17..18]
		millis = str[20..-1]

		Time.mktime(year, month, day, hour, minutes, seconds, millis)  
	end

end #end class

def main_imt_create_acrivity
	write_company_log "chamada/verificacao feita aqui, em main_imt_create_acrivity, pode e deve ser usada pelo step definition! Aqui, checo basicamente se nao deu excecao, se statuscode eh 200 e se servidor ecoa de volta valores corretos para o cliente."

	imc = InmetricsCreateActivity.new(ARGV[1]||90901010, ARGV[2]||'Rbtg Titulo Atitivade 01', ARGV[3]||Time.now, ARGV[4]||true)
	#set_massa_cen 'Criar atividade'
	#imc = InmetricsCreateActivity.new
	ret = imc.call
	if imc.error != nil
		write_company_log "erro, imc.error=#{imc.error}"
	elsif imc.status != 200
		write_company_log "erro, imc.status=#{imc.status}"
	else 
		imc.check_request_response_matches
	end
end

if ARGV[0]=='run'
	main_imt_create_acrivity #for isolated testing of automation code
end
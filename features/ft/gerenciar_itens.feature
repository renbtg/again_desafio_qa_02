#language: pt
#utf-8

Funcionalidade: Gerenciar itens
Como tester, quero gerenciar itens

Cenario: Listar itens concluidos
Dado que chamo servico para listar itens com sucesso
Quando verifico que status da listagem de itens ok
Entao garanto que listagem de itens contem algum item concluido


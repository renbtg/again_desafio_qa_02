CoordMode Mouse, Screen ;2017Out9 - movendo pra topo
	

	;Envget, ahk_debug_flag, TEST_AHK_DEBUGFLAG
	ahk_debug_flag=1
	if z%x_ahk_debug_flag%=z 
	{
		ahk_debug_flag=0
	}

	from_to=%1%
	from_to=from

	if z%from_to%=z
	{
		from_to=from
	}
	imgname_base=features\ahk\img_flight 

	CoordMode Pixel
	CoordMode Mouse

	


	achou_alguma=0
	qual_encontrada=-1
	Loop, 30 ; tenho 30 imagens a procurar: qualquer uma delas serve  
	{
		nomeimg=%imgname_base%_%from_to%_%A_Index%.jpg
		ImageSearch,  FoundX, FoundY, 0, 0, 6000, 6000, %nomeimg%
		if ErrorLevel = 0
		{
			achou_alguma=1
			qual_encontrada=%A_Index%
			break
		}
		else if ErrorLevel = 1
		{
			if ahk_debug_flag = 1
			{
				MsgBox, imgFind_FlightFromTo, nao encontrada imagem, indice %A_Index%, nomeimg=%nomeimg% 
			}
		}
		else if ErrorLevel = 2
		{
			if ahk_debug_flag = 1
			{
				MsgBox, imgFind_FlightFromTo, erro na execucao de ImageSearch, indice %A_Index%, nomeimg=%nomeimg% 
			}
		}
			
	}

	if achou_alguma=1
	{
		if ahk_debug_flag = 1
		{
			MsgBox, imgFind_FlightFromTo, imagem , indice %qual_encontrada%, encontrada nas posicoes (X x Y) %FoundX%x%FoundY%.
		}

		Exit, 0 
	}
	else
	{
		if ahk_debug_flag = 1
		{
			MsgBox, imgFind_FlightFromTo, nenhuma imagem encontrada
		}
		Exit, 1 
	}
	
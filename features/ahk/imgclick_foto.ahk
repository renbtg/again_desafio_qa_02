CoordMode Mouse, Screen ;2017Out9 - movendo pra topo
MouseMove, 50, 50,20 ;2017Out9 - movendo pra quase topo, 50/50 , velocidade 20. Antes: movia 0,0
	


	; NAO SETAR CoordMode!!!!! USAR COORDENADA JANELA ATUAL! @@@;
	; TODO 2017Jul28 - ler path-base de VARIAVEL DE AMBIENTE ou RECEBER COMO PARAMETRO
	imgname_base=features\ahk\imgclick_imgfoto_ 

	CoordMode Pixel
	CoordMode Mouse

	
	x_offset=%1%
	y_offset=%2%

	if z%x_offset%=z
		x_offset=0
	if z%y_offset%=z
		y_offset=0


	achou_alguma=0
	qual_encontrada=-1
	Loop, 30 ; tenho 30 imagens a procurar: qualquer uma delas serve; 2017Set08-fonte criado  
	{
		nomeimg=%imgname_base%%A_Index%.png
		ImageSearch,  FoundX, FoundY, 0, 0, 3000, 3000, %nomeimg%
		if ErrorLevel = 0
		{
			achou_alguma=1
			qual_encontrada=%A_Index%
			break
		}
		else if ErrorLevel = 1
		{
			;MsgBox, P13 ImageSearch, nao encontrada imagem, indice %A_Index%, nomeimg=%nomeimg% 
		}
		else if ErrorLevel = 2
		{
			;MsgBox, P13 ImageSearch, erro na execucao de ImageSearch, indice %A_Index%, nomeimg=%nomeimg% 
		}
			
	}

	if achou_alguma=1
	{
		;MsgBox, P13 ImageSearch, imagem , indice %qual_encontrada%, encontrada nas posicoes (X x Y) %FoundX%x%FoundY%.
		
		MouseMove,%FoundX%,%FoundY%,20        ;speed: de DEFAULT para 20
		sleep , 1000 ;2017Out20 - dormindo 2 segundos antes do move relativo
		MouseMove,%x_offset%,%y_offset%,20,R  ;mudada velocidade, de 0 para 20 
		
		sleep , 2000 ;2017Out20 - dormindo 2 segundos antes do click (antes: 1 seg)
		
		Click
		if ErrorLevel = 0
		{
			Exit, 0			
		}
		else
		{
			Exit, ErrorLevel
		}
	}
	else
	{
		;MsgBox, P13 ImageSearch, nenhuma das imagens foi encontrada ou problemas buscando imagens ; NAO MOSTRE ;MSGBOX!
		Exit, 1 
	}
	
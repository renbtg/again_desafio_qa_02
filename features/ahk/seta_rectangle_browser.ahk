	#WinActivateForce
	SetTitleMatchMode, 3 ;2017Set20 - match exato!

	cmdline_param1=%1%
	cmdline_paramX=%2%
	cmdline_paramY=%3%
	cmdline_paramW=%4%
	cmdline_paramH=%5%

	ex1 := WinExist(cmdline_param1)
	ex2 := WinExist("ahk_id " . cmdline_param1) 
	If ex1 or ex2
	{
		;MsgBox, VAI WinRestore janela de nome=%cmdline_param1%
		if ex1
			WinRestore,%cmdline_param1%
		if ex2
			WinRestore,ahk_id %cmdline_param1%
		if ErrorLevel <> 0
		{
			;MsgBox, AutoHK - erro ao chamar Winrestore , %cmdline_param1%
			
			Exit, 1 #2017Set20 - exit codes previsiveis
		}
		else
		{
			;MsgBox, VAI MOVER/RESIZE janela de nome=%cmdline_param1%
			if ex1
				Winmove,%cmdline_param1%,,%cmdline_paramX%,%cmdline_paramY%,%cmdline_paramW%,%cmdline_paramH%
			if ex2
				Winmove,ahk_id %cmdline_param1%,,%cmdline_paramX%,%cmdline_paramY%,%cmdline_paramW%,%cmdline_paramH%

			if ErrorLevel <> 0
			{
				;MsgBox, AutoHK - erro ao chamar Winmove
				
				Exit, 2 #2017Set20 - exit codes previsiveis
			}
			else
			{
				Exit, 0  #2017Set20 - exit codes previsiveis
			}
		}
	}
	else
	{
		;MsgBox, AutoHK ERR - nao encontrada janela de nome %cmdline_param1%
		Exit, 3  #2017Set20 - exit codes previsiveis
	}

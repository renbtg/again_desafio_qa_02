CoordMode Mouse, Screen ;2017Out9 - movendo pra topo
	

	Envget, ahk_debug_flag, TEST_AHK_DEBUGFLAG
	if z%x_ahk_debug_flag%=z 
	{
		ahk_debug_flag=0
	}

	imgname_base=features\ahk\img_tfc_logon 

	CoordMode Pixel
	CoordMode Mouse

	
	fabrica_type_str=%1%

	if z%fabrica_type_str%=z
	{
		fabrica_type_str='leanft'
	}


	achou_alguma=0
	qual_encontrada=-1
	Loop, 30 ; tenho 30 imagens a procurar: qualquer uma delas serve  
	{
		nomeimg=%imgname_base%_%fabrica_type_str%_%A_Index%.png
		ImageSearch,  FoundX, FoundY, 0, 0, 3000, 3000, %nomeimg%
		if ErrorLevel = 0
		{
			achou_alguma=1
			qual_encontrada=%A_Index%
			break
		}
		else if ErrorLevel = 1
		{
			if ahk_debug_flag = 1
			{
				MsgBox, tfc_imglogon_find, nao encontrada imagem, indice %A_Index%, nomeimg=%nomeimg% 
			}
		}
		else if ErrorLevel = 2
		{
			if ahk_debug_flag = 1
			{
				MsgBox, tfc_imglogon_find, erro na execucao de ImageSearch, indice %A_Index%, nomeimg=%nomeimg% 
			}
		}
			
	}

	if achou_alguma=1
	{
		if ahk_debug_flag = 1
		{
			MsgBox, tfc_imglogon_find, imagem , indice %qual_encontrada%, encontrada nas posicoes (X x Y) %FoundX%x%FoundY%.
		}

		Exit, 0 
	}
	else
	{
		if ahk_debug_flag = 1
		{
			MsgBox, tfc_imglogon_find, nenhuma imagem encontrada
		}
		Exit, 1 
	}
	
Envget, automdir, TEST_AUTOMATION_DIR
StringReplace, automdir, automdir, /, \, All ;2017Set8
nome_janela_upload=%1%
arquivo_imagem=%2%
txarq=%automdir%\features\ahk\%arquivo_imagem%

setKeyDelay, 100 ; 2018Set19 - 100 milissegundos = 1 decimo de segundo. Pouca demora adicionada, aumenta robustez
Loop, 100
{
	;2018Mar8 - revamped
	IfWinExist,%nome_janela_upload%
	{
		WinActivate,%nome_janela_upload%
		ControlClick, "ahk_class Edit1"
		send,%txarq%
		sleep, 2500 ; 2018Abr22 - multiplicado por 5 0 tempo total, agora, 100*2.5=250 segundos, antes, 100*0.5=50 segundos. VMs lentas? Webdesk lento? ANYWAY - levando > 100 segundos para reagir ao clique em UPLOAD DE IMAGEM+ABRIR FILECHOOSE DIALOG DO WINDOWS!
		send {ENTER}
		return 0
	}
	sleep, 300
}

;2018Mar8 - em desespero de causa, tenta escrever texto onde estiver.
send,%txarq%
sleep, 500
send {ENTER}

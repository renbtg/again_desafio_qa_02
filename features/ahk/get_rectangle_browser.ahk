	#WinActivateForce
	SetTitleMatchMode, 3 ;2017Set20 - match exato!

	Envget, ahk_out_file, TEST_AHK_OUTFILE

	cmdline_param1=%1%

	ex1 := WinExist(cmdline_param1)
	ex2 := WinExist("ahk_id " . cmdline_param1) 
	if ex1 or ex2
	{
		;MsgBox, VAI WinGetPos janela de nome=%cmdline_param1%
		if ex1
			WinGetPos,x,y,width,heigth,%cmdline_param1%
		if ex2
			WinGetPos,x,y,width,heigth,ahk_id %cmdline_param1%

		if ErrorLevel <> 0
		{
			MsgBox, AutoHK - erro ao chamar WinGetPos
			
			Exit, 1 #2017Set20 - exit codes previsiveis
		}
		else
		{
			;MsgBox, VAI ESCREVER %width%@%heigth% para arq output
			FileDelete, %ahk_out_file%
			if ErrorLevel <> 0 
			{
				;MsgBox, erro ao deletar .out
			}
			FileAppend,%x%@%y%@%width%@%heigth%,%ahk_out_file%

			if ErrorLevel <> 0
			{
				;MsgBox, AutoHK - erro %errorlevel% ao escrever pra arq output
				
				Exit, 2 #2017Set20 - exit codes previsiveis
			}
			else
			{
				Exit, 0  #2017Set20 - exit codes previsiveis
			}
		}
	}
	else
	{
		;MsgBox, AutoHK ERR - nao encontrada janela de nome %cmdline_param1%
		Exit, 3  #2017Set20 - exit codes previsiveis
	}

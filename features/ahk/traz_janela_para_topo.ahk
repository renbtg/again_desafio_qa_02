	;2017Out3 - mudado de match de TITULO para AHK_ID !
	#WinActivateForce
	cmdline_param1=%1%
	cmdline_param2=%2%
	
	match_inexato:=cmdline_param2
	
	
	janela=%cmdline_param1%
    ;MsgBox, match_inexato=%match_inexato%
    
    If match_inexato = 1 
    {
    	;MsgBox, 'DDD match_inexato=%match_inexato%, INEXATO, setting title match mode para valor 2'
	    SetTitleMatchMode, 2 ;2017Set20 - match exato!
    }
    else
    {
    	;MsgBox, 'ABC match_inexato=%match_inexato%, EXATO, setting title match mode para valor 3'
        SetTitleMatchMode, 3 ;2017Set20 - match exato!
    }
	par_array := StrSplit(cmdline_param1, "@")
	; rbattaglia,2017Out3, preparado pra receber varias janelas separadas por '@' no unico parametro
	pmt := par_array.MaxIndex()
	;MsgBox, traz_janela_pra_topo.ahk, par_array.MaxIndex()=%pmi%
	er := 0
	Loop % par_array.MaxIndex() 
	{
		janela := par_array[a_index]
		;MsgBox, traz_janela_pra_topo.ahk, Checando se janela =%janela% existe
	
		ex1 := WinExist(janela)
		ex2 := WinExist("ahk_id " . janela) 
		If ex1 or ex2
		{
			;MsgBox, traz_janela_pra_topo.ahk, VAI ATIVAR janela de nome=%janela%
			if ex1
			{
				;Msgbox, traz_topo act1
				WinActivate, %janela%
			}
			if ex2
			{
				;Msgbox, traz_topo act1
				WinActivate, ahk_id %janela%
			}
			if ErrorLevel <> 0
			{
				;MsgBox, traz_janela_pra_topo.ahk,  erro ao chamar WinActivate %janela%
				;Exit, 1 #2017Set20 - exit codes previsiveis
				er := 1
			}
			else
			{
				;MsgBox, traz_janela_pra_topo.ahk, AutoHK - ok ao trazer janela %janela% pra topo
				;Exit, 0  #2017Set20 - exit codes previsiveis
			}
		}
		else
		{
			;MsgBox, traz_janela_pra_topo.ahk,  ERR - nao encontrada janela de nome %janela%
			;Exit, 2  #2017Set20 - exit codes previsiveis
			er := 2
		}
	}
	Exit, er
	#WinActivateForce
	;MsgBox, COMECANDO MAXIMIZAR
	SetTitleMatchMode, 3 ;2017Set20 - match exato!

	cmdline_param1=%1%

	ex1 := WinExist(cmdline_param1)
	ex2 := WinExist("ahk_id " . cmdline_param1) 
	;MsgBox, MAXIM ex1=%ex1%
	;MsgBox, MAXIM ex2=%ex2%
	
	If ex1 or ex2
	{
		;MsgBox, VAI MAXIMIZAR janela de title/handle=%cmdline_param1%
		if ex1
		{
			WinMaximize, %cmdline_param1%
		}
		if ex2
		{
			WinMaximize, ahk_id %cmdline_param1%
		}

		if ErrorLevel <> 0
		{
			;MsgBox, AutoHK - erro ao chamar WinMaximize %cmdline_param1%
			Exit, 1 #2017Set20 - exit codes previsiveis
		}
		else
		{
			;MsgBox, AutoHK - OK
			Exit, 0  #2017Set20 - exit codes previsiveis
		}
	}
	else
	{
		;MsgBox, AutoHK ERR - nao encontrada janela de nome %cmdline_param1%
		Exit, 2  #2017Set20 - exit codes previsiveis
	}

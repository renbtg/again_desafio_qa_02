	#WinActivateForce
	SetTitleMatchMode, 3 ;2017Set20 - match exato!
	cmdline_param1=%1%
	;MsgBox, traz_janela_pra_topo.ahk, cmdline_param1=%cmdline_param1%
	janela=%cmdline_param1%

	par_array := StrSplit(cmdline_param1, "@")
	; rbattaglia,2017Out3, preparado pra receber varias janelas separadas por '@' no unico parametro
	pmt := par_array.MaxIndex()
	;MsgBox, traz_janela_pra_topo.ahk, par_array.MaxIndex()=%pmi%
	er := 0
	Loop % par_array.MaxIndex() 
	{
		janela := par_array[a_index]
		;MsgBox, traz_janela_pra_topo.ahk, Checando se janela =%janela% existe
	
		IfWinExist, %janela%
		{
			;MsgBox, traz_janela_pra_topo.ahk, VAI ATIVAR janela de nome=%janela%
			WinActivate, %janela%
			if ErrorLevel <> 0
			{
				
				;MsgBox, traz_janela_pra_topo.ahk,  erro ao chamar WinActivate %janela%
				;Exit, 1 #2017Set20 - exit codes previsiveis
				er := 1
			}
			else
			{
				;MsgBox, traz_janela_pra_topo.ahk, AutoHK - ok ao trazer janela %janela% pra topo
				;Exit, 0  #2017Set20 - exit codes previsiveis
			}
		}
		else
		{
			;MsgBox, traz_janela_pra_topo.ahk,  ERR - nao encontrada janela de nome %janela%
			;Exit, 2  #2017Set20 - exit codes previsiveis
			er := 2
		}
	}
	Exit, er
Dado (/^que chamo servico para listar itens com sucesso$/) do
	@ica = InmetricsTypiCode.new
	@ica.call
end

E(/^verifico que status da listagem de itens ok$/) do
	@ica.check_status_ok
end

Entao(/^garanto que listagem de itens contem algum item concluido$/) do
	@ica.check_has_complete_true
end

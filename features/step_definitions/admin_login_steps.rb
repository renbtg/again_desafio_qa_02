Quando(/^Abro pagina de login$/) do
	ImcLoginPage.new.process_start
end

E(/^preencho usuario e senha$/) do
	ImcLoginPage.new.process_fillin
end

Entao(/^submeto e concluo login com sucesso$/) do
	ImcLoginPage.new.process_submit
end

require_relative "./utils.rb"

include Capybara::DSL

=begin
TODO 2017Ago10 
	done 1) cucumber.yml já salva HTML e JSON
	todo 2) veja q.rtf , ^SÒ FALTA PARALELISMO E REPORT FINAL!
		TODO 2.a) prioridade inicial - paralelismo )OBS: nao se preocupe, cucuber.yml já vai salvar JSONs isolados)
		TODO 2.b) gema report.blabla, pra ler JSONs e gerar um report só
		TODO 2.c) achar jeito de linkar SCREENSHOT HTML em vez de SCREENSHOT PNG nos reports (html e json)
		TODO 2.d) segregar GERACAO da EXECUCAO, pode requerer conceitos novos de "artefatos intermediários"
=end

write_company_log :debug, 'env.rb, antes de register_driver'
Capybara.register_driver :selenium do |app| #Eu chamei de :selenium pela gema (agora nao usado) capybara-screenshot
	client = Selenium::WebDriver::Remote::Http::Default.new
  	client.open_timeout = 120 #2017Ago13 - visit, mais timeout: passar client em httpc_lient pra Capybara::Selenium::Driver.new 
  	client.read_timeout = 120 #2017Ago13 - visit, mais timeout: passar client em httpc_lient pra Capybara::Selenium::Driver.new 
  	write_company_log :debug, 'DO block de env.rb, register_driver, chamado!'
	retval = Capybara::Selenium::Driver.new(app, :browser => :chrome, :http_client => client)
	write_company_log :debug, "passou pelo Capybara::Selenium::Driver.new"
	#Capybara::Selenium::Driver.new(app, :browser => :internet_explorer)
	retval
end
write_company_log :debug, 'env.rb, depois de register_driver'

	Capybara.default_driver = :selenium
	Capybara.javascript_driver = :selenium

Capybara.configure do |config|
	config.default_max_wait_time = 20 #PRA ATUALIZACOES AJAX

end

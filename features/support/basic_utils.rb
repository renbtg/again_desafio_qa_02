require_relative  './util_spreadsheet.rb'

def get_test_process_num
	return get_process_num
end

def is_str_integer?(str)
	return false if not str
	return str.scan(/\D/).empty?
end
def is_str_alpha?(str)
	return false if not str
	return str.match(/^[[:alpha:]]+$/) != nil
end
def is_str_alphanumeric?(str)
	return false if not str
	return str == get_alphanumeric(str)
end

def get_alphanumeric(str)
	return nil if not str
	return str.gsub(/[^0-9a-z]/i, '')
end

def get_alphanumcenario(nome_cenario)
	nome_cenario
end

def real_get_alphanumcenario(nome_cenario)
	#NUNCA EM HIPOTESE NENHUMA CHAm write_company_log aqui, pois write_company_log chama esta rotina!
	retval=""
	begin
		if ENV['TEST_ALPHANUM_FEATURENAME']
			retval = "V #{ENV['TEST_ALPHANUM_FEATURENAME']}"
		else
			alphacen = 'V ' + get_alphanumeric( nome_cenario[('V '.length)..(-1)])
			retval = alphacen
		end
	rescue Exception #=> e
		#nada
	end
	return retval
end



def get_paralelismo
	return (ENV['TEST_PARALELISMO']||'1').to_i
end

def get_process_num
	return (ENV['TEST_PROCESS_NUM']||'1').to_i
end




def obtem_guid
 	real_guid=SecureRandom.uuid
 	return real_guid
end

def obtem_alphanum_guid(nchars=nil)
 	if nchars==0
 		nchars=nil
 	end
 	real_guid=obtem_guid
 	fullguid= real_guid.gsub('-','')
 	nchars=nchars || (fullguid.length)
 	#puts "fullguid=#{fullguid}"
 	#puts "nchars=#{nchars}"
 	return fullguid[(-1*nchars)..-1]
end
 	
def obtem_guid_reduzido()
 	real_guid=obtem_guid
 	return real_guid.split('-').last[-3..0]
end


def delete_all_in_dir(dir_path)
	#write_company_log :debug, "delete_all_in_dir pra dir_path=#{dir_path} chamada" 
	FileUtils.rm_rf Dir.glob("#{dir_path}/*")
end	

def criadir_senaoexistir(some_path)
	FileUtils.mkdir_p(some_path)
end
def mkdir_noexist(some_path)
	FileUtils.mkdir_p(some_path)
end


def salva_arquivo_xls(xls_file, colnames, hash_array)
	sep=';'

	if colnames == nil
		todas_chaves=[]
		hash_array.each do |h| 
			# nao podemos ter linhas com hash de strutura diferente
			todas_chaves = todas_chaves | h.keys
		end
		hash_array.each do |h|
			#equalizamos aqui os hashs, devem todos ter as mesmas chaves
			chaves_ausentes = todas_chaves - h.keys
			chaves_ausentes.each do |ausente|
				h[ausente] = '' #adiciona chave ausente, sel valor
			end 
		end
		colnames = todas_chaves
		n_hash_array = Array.new
		hash_array.each do |h|
			#remonta o hash array com dodas as chaves presentes e em mesma ordem
			nh = Hash.new
			colnames.each do |colname|
				nh[colname] = h[colname]
			end
			n_hash_array << nh
		end
		hash_array = n_hash_array

	end

	dir_arqs=File.dirname xls_file
	nome_xls = dir_arqs + '/' + File.basename(xls_file)
	nome_csv = dir_arqs + '/' + File.basename(xls_file,'.xls')+'.'+obtem_alphanum_guid+".csv"
	# nao usa diretorio, sempre em Planilhas e Controles

	#write_company_log :debug, "reescreve_xls_risco: nome_xls=#{nome_xls}, nome_csv=#{nome_csv}"
	f_m=nil
	begin
	 	f_m = File.open(nome_csv, "wt")

		f_m.puts colnames.join(';')

		hash_array.each do |linha|
			vlrs = Array.new
			colnames.each do |col|
				#write_company_log :debug, "adicionando col=#{col}"
				#write_company_log :debug, "adicionando linha=#{linha}"
				vlrs << linha [col]
			end
			f_m.puts vlrs.join ';'
		end
	rescue Exception => e
		f_m.close if f_m != nil
		falhar e
	ensure
		f_m.close if f_m != nil
	end

	book = Spreadsheet::Workbook.new
	sheet1 = book.create_worksheet
	header_format = Spreadsheet::Format.new(
	  :weight => :bold,
	  :horizontal_align => :center,
	  #:bottom => true, #exemplo q vi bottom invalido
	  :locked => true
	)
	sheet1.row(0).default_format = header_format
	#system 'type csv_massa.txt'
	#write_company_log :debug, "vai manipular csv"
	CSV.open(nome_csv, 'r', :col_sep=>sep) do |csv|
		csv.each_with_index do |row, i|
	    	#write_company_log :debug, "manipulando linha #{i}"
	    	sheet1.row(i).replace row
	  	end
	end
	FileUtils.rm nome_csv
	book.write(nome_xls)
	return
end

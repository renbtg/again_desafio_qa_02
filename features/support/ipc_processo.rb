require_relative './company_log.rb'
require  'os'

def processo_naofilho_executando? (pid_or_where)
	if OS.linux?
		pid = pid_or_where
		return linux_processo_naofilho_executando? (pid)
	else
		return windows_processo_naofilho_executando? pid_or_where
	end
end

def mata_processo_sigkill (pid)
	Process.kill 9, pid
end

def mata_todos_processos(where) 
	if OS.linux?
		falhar 'nao implementado'
	else
		windows_mata_todos_processos(where)
	end
end


def get_processos_filhos_terminados
	if not defined? $processos_filhos_terminados
		$processos_filhos_terminados=[]
	end
	return $processos_filhos_terminados
end

def registra_termino_de_processo_filho()
	get_processos_filhos_terminados << $?
end

def get_pids_filhos_terminados
	return get_processos_filhos_terminados.map{|p| p.pid}
end
def processo_filho_ja_terminado?(pid)
	return get_pids_filhos_terminados.include? pid
end

def processo_executando? (pid)
	#puts "processo_executando?(#{pid})"
	retval = processo_filho_executando? pid
	if retval == nil
		retval = processo_naofilho_executando? pid
	end
	return retval
end

def processo_filho_executando? (pid)
	#puts "processo_filho_executando?(#{pid})"
	return false if processo_filho_ja_terminado? pid

	begin
		p = Process.waitpid( pid, Process::WNOHANG) 
		#puts "processo_executando?, p=#{p}"
		if p != nil
			registra_termino_de_processo_filho
		end
		return p == nil
	rescue Errno::ECHILD => e
		puts "processo_executando? pid=#{pid}, Errno::ECHILD, ja morreu ou NAO_FILHO"
		falhar e 
		#NAO devo fazer RAISE, pois processo pai pode chamar por engano varias vees
		#
		#
		## LÒGICO...posso fazer  
	end
end

def imprime_pids_processos (pids)
	write_company_log "imprime_pids_proessos, parametro pids=#{pids}"

	pids.each do |pid|
		if processo_executando? pid
			write_company_log :debug, "pid #{pid} ta vivo"
		else
			write_company_log :debug, "pid #{pid} morreu"
		end
	end
	write_company_log :debuFg, "Aqui, ja rolou WAIT. Cheque o que deu com filhos mortos... $processos_filhos_terminados=#{$processos_filhos_terminados}"
end

def windows_mata_todos_processos(where, nof_tries = 15)

	c="wmic path win32_process where '#{where}' get ProcessId"
	#puts "QQc=#{c}"

#		c="wmic path win32_process where '#{where}' get ProcessId, CommandLine"

	o = run_command_retry_eagain(c, nof_tries)

	#
	#
	# WTF! KILLING It was disabled by a "return" statement early in this method. FREAKING IDIOCY!
	#
	#

	write_company_log :debug, "windows_mata_todos_processos(where=#{where}), c=#{c}, o=#{o}"
	#puts "o=#{o}"
	return if not o #2017Mar14, junto com refactoring pra trocar ` por system >bla.txt, read txt

	# = o[2..-1].force_encoding('cp850')
	#o = o[2..-1] #if o[0] != 'P' #2018Mar14, pulando BOMB CHAR	

	write_company_log("windows_mata_todos_processos(where=#{where}), antes de split, o=#{o}, o.encoding=#{o.encoding}")

	aa=o.split("\n".encode('utf-8')).select{|e|e!=''}
	#aa=o.select{|e|e!=''}
	write_company_log("windows_mata_todos_processos(where=#{where}), depos de split, aa= #{aa}")
	if (aa.first||'').include?  'ProcessId'
		write_company_log("windows_mata_todos_processos(where=#{where}), achou ProcessId")
		(aa.length-1).times do |k|
			write_company_log("windows_mata_todos_processos(where=#{where}), k=#{k}, aa.length=#{aa.length}")
			pid=aa[k+1].to_i #2018Mar14, agora tem espaços no meio do pid, entre cada digito! esquisitao
			begin
				write_company_log "Vai chamar... Process.kill 9, #{pid}"
				Process.kill 9, pid
			rescue SystemCallError, Errno::EIO => e
				#ignorando puts "classe da excecao=#{e.class}, excecao=#{e}"
				write_company_log :debug, "windows_mata_todos_processos('#{where}'), classe da excecao=#{e.class}, excecao=#{e}"
			end
			if OS.windows?
				#2018Out11 - depois do 'Process.kill', também chama 'wmic process delete' que mata/remove processos residuais. MAIS SEGURO que apenas substituir 'Process.kill' por 'wmic process delete' 
				#2018Out10 - modo mais radical de matar, uma vez "cmd /cblabla copiar bla ahk_TPNbla.exe" de tfc_imglogon.bmp ficou travado para sempre, multiplos kills de várias formas não funcionaram, mas o "wmic*delete" funcionou"
				wmic_del_cmdline = "wmic process where \"ProcessId=#{pid}\" delete"
				write_company_log :debug, "just_kill, vai execucat comando na variavel wmic_del_cmdline='#{wmic_del_cmdline}'"
				o = comando_sistema(wmic_del_cmdline)
				write_company_log :debug, "just_kill, vai execucat comando na variavel wmic_del_cmdline='#{wmic_del_cmdline}', o=#{o}"
			end
		end
	end
end

def windows_processo_naofilho_executando? (pid_or_where)
	if not pid_or_where.is_a? String
		pid =pid_or_where
		c="wmic path win32_process where \"PROCESSID = #{pid}\" get Processid 2>&1"
	else
		where = pid_or_where
		c="wmic path win32_process where '#{where}' get ProcessId, CommandLine 2>&1"
	end

	s=run_command_retry_eagain(c)
	write_company_log "windows_processo_naofilho_executando?, c=#{c}, s=#{s}"
	falhar "impossível obter output de wmic, c=#{c}, s=#{s}" if not s or s==''
	return (not (s.include? 'No Instance'))
end

def linux_processo_naofilho_executando? (pid)
	begin
	  Process.getpgid( pid )
	  return true
	rescue Errno::ESRCH
	  return false
	end
end

def clausula_wmic_where_de_pid_ou_palavras(pid_ou_palavras)
#2018Set01 - adicionado método clausula_wmic_where_de_pid_ou_palavras
	s = pid_ou_palavras
	if s.is_a? Fixnum
		where_clause = signature
	else
		all_words = s
		if s.is_a? Array
			#nada
		else
			all_words = s.split(' ')
		end
		all_words_clauses = "(COMMANDLINE like '%%'" + all_words.map{|one|" AND COMMANDLINE like '%#{one}%' "}.join(' ') + ')'; where_clause = "(#{all_words_clauses} and not COMMANDLINE like '%wmic%')"
	end
	return where_clause
end


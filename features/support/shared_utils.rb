require_relative './basic_utils.rb'
require_relative './utils.rb'
require_relative './company_log.rb'

CONST_LOCK_EXCLUSIVO_GUI = 'LOCK_EXCLUSIVO_GUI'


def falhar(msg, definitivo=false)
	if (not msg.is_a? String)
		msg = msg.to_s
	else
		#nada
	end

	new_msg=ascii_only_str(msg) #2017Nov14, protege tambem diretamente aqui

	write_company_log :error, new_msg

	fail new_msg
end

def ascii_only_str(non_ascii_string)
	replacements = { 
	#  'á' => "a",
	# @ 'ë' => 'e'
	}



	encoding_options = {
	  :replace => "*",             # Use a blank for those replacements
	  :invalid   => :replace,     # Replace invalid byte sequences
	  :universal_newline => true, # Always break lines with \n
	  # For any character that isn't defined in ASCII, run this
	  # code to find out how to replace it
	  :fallback => lambda { |char|
	    # If no replacement is specified, use an empty string
	    replacements.fetch(char, "*")
	  },
	}

	ascii = non_ascii_string.encode(Encoding.find('ASCII'), encoding_options)
	#puts ascii.inspect
	return ascii
end

def remove_nonascii_exception
	#2017Out17 - TODOS entry-points de page-object devem ser protegidos por este método
	begin
		yield
	rescue Exception => e
		write_company_log :trace, "remove_nonascii_exception, e=#{e}, e.class=#{e.class}"
		new_text=nil
		begin
			emsg=e.message
			new_text=add_app_prefix_to_msg(ascii_only_str(emsg))
		rescue Exception => xe
			write_company_log :trace, "remove_nonascii_exception - Excecao #{xe} ao obter e.message"
		end
		if new_text and new_text != e.message
			raise e, new_text, e.backtrace
		else
			raise e
		end
	end
end

def escreve_xls_de_hash(xlsname,data)
	begin
		mkdir_noexist get_automtmpdir
	rescue SystemCallError => e
		#nada
	end
	f_m = nil
	nome_tmp="#{get_automtmpdir}/csv_tmp_#{obtem_guid}.txt"

	begin
		f_m = File.open(nome_tmp, "w+t")

		data.keys.each_index do |i|
			f_m.write data.keys[i]
			if i < (data.keys.length - 1)
				f_m.write ";"
			else
				f_m.write "\n"
			end
		end
		data.keys.each_index do |i|
			k=data.keys[i]
			f_m.write data[k].to_s # ASPAS simples - sugere formato
			if i < (data.keys.length - 1)
				f_m.write ";"
			else
				f_m.write "\n"
			end
		end
	rescue Exception => e
		f_m.close if f_m != nil
		raise e
	ensure
		f_m.close if f_m != nil
	end

	book = Spreadsheet::Workbook.new
	sheet1 = book.create_worksheet
	header_format = Spreadsheet::Format.new(
	  :weight => :bold,
	  :horizontal_align => :center,
	  #:bottom => true, #exemplo q vi bottom invalido
	  :locked => true
	)
	sheet1.row(0).default_format = header_format
	CSV.open(nome_tmp, 'r', :col_sep=>';') do |csv|
		csv.each_with_index do |row, i|
	    	#write_company_log :debug, "manipulando linha #{i}"
	    	sheet1.row(i).replace row
	  	end
	end
	FileUtils.rm nome_tmp

	book.write(xlsname)
	#book.io.close #2018Mar11 - pode resolver problemas intermitentes de nao permitir edição de XLSs criados pelo programa  #DAMN, depois de write, io fica nil. FIQUE ATENTO!
	return
end

def append_line_to_file(fname, str)
	File.open(fname,'a+') { |f| f.puts("#{str}") }
	return
end
def write_text_to_file(fname, str)
	File.open(fname,'w') { |f| f.write("#{str}") }
	return
end

def strzero(inum, nchars)
	fmtstr="%0#{nchars}d"
	return sprintf(fmtstr, inum)
end

def gera_screenshot(nomeTela=nil)
	if false
		10.times {write_company_log :debug, "gera_screenshot, desabilitados, nomeTela=#{nomeTela}"}
		return
	end

    sleep 4 #rbattaglia 2018Fev06 - já tinha este sleep 4. Necessario? Sempre?
	scrshot_fullpath = "#{get_nome_de_screenshot}/#{Time.now.strftime('%y%m%d-%H%M%S')}_#{nomeTela}.browser.png"
	begin
    	page.save_screenshot scrshot_fullpath
	rescue Exception => e
		write_company_log :error, "Erro excecao=#{e} , gerando screenshot, filepath=$scrshot_fullpath"
    end
	return
end


def load_java_properties(properties_filename)
    properties = {}
    File.open(properties_filename, 'r') do |properties_file|
      properties_file.read.each_line do |line|
        line.strip!
        if (line[0] != ?# and line[0] != ?=)
          i = line.index('=')
          if (i)
            properties[line[0..i - 1].strip] = line[i + 1..-1].strip
          else
            properties[line] = ''
          end
        end
      end      
    end
    properties
end

def save_java_properties(properties_filename, hash_props)
#	write_company_log :debug, "save_java_properties P00, properties_filename=#{properties_filename}, hash_props=#{hash_props}"
	out_str=''
	hash_props.keys.each do |k|
		out_str=out_str+"#{k}=#{hash_props[k]}\n"
	end
	tmp_path = "#{get_automtmpdir}/temp.#{obtem_alphanum_guid}.properties" #2018Mar19 earlyam - estava gerando PERMISSION DENIED antes, por faltar desambiguador "obtem_alphanum_guid" no nome do arquivo temporario!
	File.open(tmp_path,'wt') { |f| f.puts out_str }
	FileUtils.mv tmp_path, properties_filename
#	write_company_log :debug, "save_java_properties retornando, properties_filename=#{properties_filename}, hash_props=#{hash_props}"
end


def out_system(cmdline)
	return `#{cmdline}` 


	tmpfname = "#{get_automtmpdir}/out_system_#{obtem_alphanum_guid}.txt"
	cmd_completo = "#{cmdline} >#{tmpfname} 2>&1}"
	system cmd_completo
	retval = File.read(tmpfname) if File.exist? tmpfname
	FileUtils.rm tmpfname if File.exist? tmpfname
	return retval
end

def create_new_file(fname, content = nil)
	retval = nil
	File.open(fname,File::WRONLY|File::CREAT|File::EXCL) { |f|
		if content
			f.write content
		end
		if block_given?
			retval = yield(f)
		end
	}
	return retval
end


def str_short_random(s_orig, lmax, preserve_words=nil)
	#TODO 2018Mai5 - nao remover caracteres que estejam no meio da palavra 'Rnd'
	preserve_words = preserve_words || Array.new
	falhar "str_short_random, parametro preserve_words=#{preserve_words} invalido" if preserve_words.class != Array

	s_new = s_orig
	#puts "check, s_orig=#{s_orig}, len=#{s_orig.length}"
	while s_new.length > lmax
		preserve_w_pos = []  #posicao de cada palavra que nao pode ser adulterada
		preserve_words.each do |w|
			i = s_new.index w
			w.length.times do |k|
				preserve_w_pos << i + k #todas as posicoes na String ocupadas pelas palavras a preservar. Exempo: s=Rnd1000020000BLABLABLAMrk25 , preserve_words=['Rnd','Mrk'], preserve_w_pos ficará [0,1,2,22,23,24], de forma a impedir que qualquer um dos caracteres com essas posições sejam removidos para reduzir comprimento da string.
			end
		end
		#puts "preserve_w_pos=#{preserve_w_pos}"

		p1=''
		p2=''
		pos_remove = nil
		visited=[] #posicoes ja visitadas. Se ja visitou todas , falha/raise 
		begin 
			if visited.length == s_new.length
				falhar "str_short_random, nao foi possivel reduzir palavra, s_orig=#{s_orig}, lmax=#{lmax}, preserve_words=#{preserve_words}, s_new=#{s_new}"  		
			end
			pos_remove = Random.new.rand(0..s_new.length-1) 
			visited << pos_remove if not visited.include?(pos_remove)
		end until not preserve_w_pos.include?(pos_remove) #loop até que tenha escolhido caracter a remover que nao esteja dentro das palavras a preservar
	
		#puts "pos_remove=#{pos_remove}"
		p1 = s_new[0..pos_remove-1] if pos_remove > 0
		#puts "p1=#{p1}"
		p2 = s_new[pos_remove+1..-1] if pos_remove < s_new.length - 1
		#puts "p2=#{p2}"
		s_new = "#{p1}#{p2}"
		#puts "agora, s_new =#{s_new}, len=#{s_new.length}"
		falhar 'Erro interno de implementacao' if s_new.length > s_orig.length 
	end
	s_new
end

def run_command_retry_eagain(c, nof_tries=15)
	o=nil
	nof_tries.times do |try_num| 
		begin 
			o = out_system(c)
			break
		rescue Errno::EAGAIN => eagain
			write_company_log :debug, "Excecao #{eagain} em run_command_retry(c='#{c}''), tentativa #{try_num+1} de #{nof_tries}"
			if try_num+1 == nof_tries
				raise eagain
			end
		end
	end
	return o
end

def comando_sistema(s_cmd, extrap={:cmd_type => :system})
#2018Set01 - created
#TODO 2018Set01 - adicionar opcao :cmd_type==:spawn, com controle de timeout. Nao deve aceitar spawn sem timeout? Talvez... talvez, deva aceitar, só que precisaria de BLOCK a ser executado em cada iteracao de checagem, algo assim.

	raise "comando_sistema, apenas opcao :system sem timeout implementada até 2018Set01" if not (extrap and extrap.is_a? Hash and extrap.keys.length == 1 and extrap[:cmd_type]==:system)
	cmd_out_txt = "#{get_automtmpdir}/cmd_output_#{obtem_alphanum_guid(6)}.txt".gsub('/','\\')
	#2018Out8 - bugfix, cmd_out_txt deve usar barras invertidas '\' do Windows
	full_cmdline = "#{s_cmd} > #{cmd_out_txt} 2>&1"
	write_company_log :debug, "comando_sistema(s_cmd=#{s_cmd}), full_cmdline=#{full_cmdline}"
	system full_cmdline #TENHO: TRN1 anterior unzipado em diretorio temporario  
	o = File.read cmd_out_txt if File.exist? cmd_out_txt
	FileUtils.rm cmd_out_txt if File.exist? cmd_out_txt
	return o
end

def get_segundos_fila_lock_exclusivo(ctx='LOCK_EXCLUSIVO_GUI')	
	# TODO 2018Fev10 - definir algoritmo melhor. POR FAVOR, REFINE! Timesouts de paralelismo estão todos meio chutados, sem muito método. Devem levar em conta performance da máquina e tudo, algum aqruivo CFG que pode até ser mais refinado com auto-detecção (ZS_PROC? setado manual? ANYWAY: POR FAVOR, REFINE!)
	tempo_default_operacao_exclusiva = 60*3

	if ctx == CONST_LOCK_EXCLUSIVO_GUI
		#paralelismo = get_paralelismo
		#tout = 60 * paralelismo #4 processos=4 min... 16=16min  !! ?? NAO SERIA MELHOR FOREVER? 100000?
		#tout = [tout, tempo_tfc_operacao_exclusiva].max #2018Fev10, TFC
		
		#return tout
		return 750 #um coitado tá esperando 3 TFCs meio-lerdos... de 250 segs cada

####EITA PREUGA! 2018Fev25 - Que TEMPO foi aquele que fez "desistir" de LOCK no TFC, deixando outras pagias web passarem por cima em conflito? 

	elsif ctx == 'LOCK_FILEMANAGER'
		return 30*3 #30 segundos, que exagero! CREIO que posso reduzir pra 5... TENTAR, se performance tiver gargalo por isso! 
	end
	return tempo_default_operacao_exclusiva 
end

def get_lock_file_name(ctx)
	return "#{get_automdir}/LAST.#{ctx}.#{get_process_num}.FILA.LCK"
end
def get_todos_lock_file_names(ctx)
	ret=nil
	executa_exclusivo(nil, 'LOCK_MANAGER') do
		ret = Dir["#{get_automdir}/LAST.#{ctx}.*.FILA.LCK"]
	end
	return ret
end

def registra_atendendo_fila_lock(ctx)
	executa_exclusivo(nil, 'LOCK_MANAGER') do
		fname = get_lock_file_name(ctx)
		(ordem_precedencia , ftime)=File.read(fname).split(";").map(&:strip)
		write_rsi_log :debug, "registra_atendendo_fila_lock(#{ctx}): trocando a ordem_precedencia que era #{ordem_precedencia} para ZERO(0)=primeirissima precedencia"
		File.open(fname,'w') { |f| f.puts "%09d;%018.6f" % [0,ftime] }
	end
end


def registra_fila_lock(ctx, ordem_precedencia=0)
	executa_exclusivo(nil, 'LOCK_MANAGER') do
		fname = get_lock_file_name(ctx)

		loop do 
			excecao = nil
			begin
				File.open(fname,'w') {|f|
					f.puts "%09d;%018.6f" % [ordem_precedencia,Time.now.to_f] 
					# já grava em formato bom para SORT simplificado, ao comparar
					#conteúdo de vários arquivos
					#
					#ordem_precedencia: é checada antes, quando menor 
				}
			rescue Exception => e
				excecao = e
			end
			if excecao
				write_rsi_log :warn, "registra_fila_lock(#{ctx}, #{ordem_precedencia}): tentando de novo, excecao #{e} ao criar/escrever arquivo #{fname}"
			else
				break
			end
		end
	end
end

def remove_fila_lock(ctx, chamadora=nil)
	executa_exclusivo(nil, 'LOCK_MANAGER') do
		fname = get_lock_file_name(ctx)
		excecao = nil
		begin
			if File.file? fname 
				FileUtils.rm fname
			else
				write_rsi_log :debug, "remove_fila_lock(#{ctx}, #{chamadora}): nao encontrado arquivo #{fname}"
			end
		rescue Exception => e
			excecao = e
			write_rsi_log :debug, "remove_fila_lock(#{ctx}, #{chamadora}): NAO TENTAREI DE NOVO, ignorando excecao, excecao #{e}"
		end
	end
end

def remove_todos_fila_lock
	executa_exclusivo(nil, 'LOCK_MANAGER') do
		Dir["#{get_automdir}/*.FILA.LCK"].each{|fname|
			FileUtils.rm fname 
		}
	end
end

def obtem_lock(ctx, ordem_precedencia=1) #nem passa parametro TIMEOUT, obtido internameente
	#
	#
	#    **  P O R   F A V O R, A T E N C A O
	#  
	#    ** N A O   C E R Q U E   T O D O    E S T E    M È T O D O   
	#       ------ 
	#       ------ 
	#    C O M    executa_exclusivo   ! ! ! !
	#


	write_rsi_log :debug, "obtem_lock: P00"
	hora_inicio_espera=Time.now
	
	executa_exclusivo(nil, 'LOCK_MANAGER') {
		registra_fila_lock(ctx, ordem_precedencia)
	
		# em paralelismo 3, os 9 processos podem se colocar na fila
	}
	
	if not File.exist? get_lock_file_name(ctx)
		write_rsi_log :error, "ASSERTION FAILED: obtem_lock: Acabou de registrar E NAO EXISTE ARQUIVO LOCK!!! #{get_lock_file_name(ctx)}" if not File.exist? get_lock_file_name(ctx)
		File.open("#{get_automdir}/TERMINAR.LCK","w") {} rescue nil #RADICAL? ABORTA TODA A AUTOMACAO?
	end
	falhar "ASSERTION FAILED: obtem_lock: Acabou de registrar E NAO EXISTE ARQUIVO LOCK!!! #{get_lock_file_name(ctx)}" if not File.exist? get_lock_file_name(ctx)

	loop do
		ok = false
		write_rsi_log :debug, "obtem_lock: processo #{get_process_num} aguardando até ser o mais prioritario esperando, desde aproximadamente #{hora_inicio_espera}" 
		executa_exclusivo(nil, 'LOCK_MANAGER') { 
			#SIM! Lock granular a cada tentativa
			#
			# Ao ter CHECA e REGISTRA_SUCESSO dentro do mesmo LOCK, evita conflitos entre
			# os arquivos de controle, ao ser removidos/reescritos/abertos etc. por
			# outros processos

			ok =  checa_sou_prioritario_fila_lock(ctx,get_paralelismo)
			if ok
				write_rsi_log :trace, "obtem_lock: ok, funcao checa_sou_prioritario_fila_lock retornou true" 
				registra_atendendo_fila_lock ctx #OBRIGATORIO que registra_atendendo_fila_lock esteja em mesmo executa_exclusivo que checa!
			end
		}
		break if ok
		sleep 0.5
	end

end

def checa_sou_prioritario_fila_lock(ctx, entre_quantos_mais_prioritarios)
	ret = nil
	executa_exclusivo(nil, 'LOCK_MANAGER') do
		ret = core_checa_sou_prioritario_fila_lock(ctx, entre_quantos_mais_prioritarios) 
	end

	return ret
end

def core_checa_sou_prioritario_fila_lock(ctx, entre_quantos_mais_prioritarios)
	a = get_todos_lock_file_names(ctx).map { |fname| 
		ret=nil
		fname.upcase!
		#write_rsi_log "checa_sou_prioritario_fila_lock: fname=#{fname}"
		fnum=fname.split(".FILA.LCK").first.split(".").last.to_i
		File.open(fname) { |fhandle|
			#write_rsi_log "checa_sou_prioritario_fila_lock: wanna-be fnum=#{fnum}"
			ret = {
				:fnum=>fnum,
				:conteudo=>fhandle.readline
			} 
		}

		ret
	}.select{ |e|
		e != nil
	}.sort { |e1, e2|
		c1=e1[:conteudo]
		c2=e2[:conteudo]
		#write_rsi_log :debug, "checa_sou_prioritario_fila_lock: comparacao, e1=#{e1}, c2=#{c2}"
		ret=c1 <=> c2
		#write_rsi_log :debug, "checa_sou_prioritario_fila_lock: ret=#{ret} pra comparacao entre #{e1} e #{e2} "
		ret
	}
	#write_rsi_log :debug, "checa_sou_prioritario_fila_lock: a=#{a}"
	my_test_process_num=get_process_num
	my_index=a.find_index{|e| e[:fnum] == my_test_process_num}
	#se nao encontrar, significa que nunca teve sucesso
	#write_rsi_log :debug, "checa_sou_prioritario_fila_lock: my_index=#{my_index}, my_test_process_num=#{my_test_process_num}, entre_quantos_mais_prioritarios=#{entre_quantos_mais_prioritarios}"
	
	if my_index == nil
		falhar "ASSERTION FAILED: checa_sou_prioritario_fila_lock: nem achei eu mesmo, my_test_process_num=#{my_test_process_num}, entre os que esperam"
	end

	if my_index < entre_quantos_mais_prioritarios
		#TUDO OK, sou um dos mais prioritarios
		write_rsi_log :trace, "checa_sou_prioritario_fila_lock: eu mesmo, my_test_process_num=#{my_test_process_num}, eh um dos #{entre_quantos_mais_prioritarios} mais prioritarios, OK!, my_index=#{my_index}"
		return true
	end

	#write_rsi_log :debug, "checa_sou_prioritario_fila_lock: eu mesmo, my_test_process_num=#{my_test_process_num}, NAO EH NAO um dos #{entre_quantos_mais_prioritarios} mais prioritarios, , my_index=#{my_index}"
	return false
end

def has_lock_exclusivo(ctx = CONST_LOCK_EXCLUSIVO_GUI)
	return ($already_locked||{})[ctx] == true
end
def last_lock_exclusivo_falhou(ctx = CONST_LOCK_EXCLUSIVO_GUI)
	return ($lock_exclusivo_falhou||{})[ctx] == true
end

def executa_exclusivo(hash_param=nil, ctx=nil, segundos_espera_flock = nil)
# executa_exclusivo hash_param: :maximizar_janela_antes , :trazer_janela_para_topo_antes, :restore_janela_depois	# executa_exclusivo hash_param: :maximizar_janela_antes , :trazer_janela_para_topo_antes, :restore_janela_depois# executa_exclusivo hash_param: :maximizar_janela_antes , :trazer_janela_para_topo_antes, :restore_janela_depois
	ctx ||= CONST_LOCK_EXCLUSIVO_GUI

	hash_param ||= {}

	$already_locked ||= {}
	$lock_exclusivo_falhou ||= {}
	
	$lock_exclusivo_falhou[ctx] = false  

	valid_hash_keys=[
			:maximizar_janela_antes , 
			:trazer_janela_para_topo_antes, 
			:restore_janela_depois,
			:verbose,
			:segundos_sleep

	]

	useless_keys = []
	if janelas_sempre_maximizadas?
		useless_keys = useless_keys + [
			:maximizar_janela_antes , 
			#2018Jan19 - :trazer_janela_para_topo_antes, 
			:restore_janela_depois
		]
	end
	
	if not $not_single_maq_zsproc_debug
		useless_keys = valid_hash_keys
	end

	
	useless_keys.each {|k| hash_param[k] = false if hash_param[k] != nil}

	hash_param.keys.each do |key|
		if not valid_hash_keys.include? key
			falhar "executa_exclusivo - Erro - chave errada (class,value)=(#{key.class},#{key}) no hash de parametros, chaves validas=#{valid_hash_keys}"
		end	
	end

	dirpath_lock_exclusivo_semaforo = ''; suffix_lock_exclusivo_semaforo = ''; dirpath_lock_exclusivo_semaforo = "#{get_automdir}/" if File.basename(ctx) == ctx; suffix_lock_exclusivo_semaforo = ".LCK"  if File.extname(ctx) == ''; nomearq_lock_exclusivo_semaforo = dirpath_lock_exclusivo_semaforo + ctx + suffix_lock_exclusivo_semaforo
	
	#write_rsi_log :trace, "executa_exclusivo(ctx=#{ctx}): 00 = defined? #{$already_locked} = #{defined? $already_locked}"
	if $already_locked[ctx] == nil
		write_rsi_log :trace, "executa_exclusivo: $already_locked[#{ctx} INDEFINIDO" if false #2018Set26 - menos log
		$already_locked[ctx] = false
	end
	write_rsi_log :trace, "executa_exclusivo(ctx=#{ctx}): 01 = $already_locked #{$already_locked}"  if false #2018Set26 - menos log

	hash_param = hash_param || Hash.new
	
	fez_lock_agora = false

	if not $already_locked[ctx]
		segundos_espera_flock ||= get_segundos_fila_lock_exclusivo(ctx)
		if segundos_espera_flock == -1
			segundos_espera_flock = 365*24*60*20 #um ano, 30 dias, 24 horas, 60 minutos, 60 segundos
		end
		write_rsi_log :trace, "executa_exclusivo:  antes de tentar abrir/criar arquivo #{nomearq_lock_exclusivo_semaforo}"  if false #2018Set26 - menos log 
		f = File.open(nomearq_lock_exclusivo_semaforo, "w")
		write_rsi_log :trace, "executa_exclusivo: depois de tentar abrir/criar arquivo #{nomearq_lock_exclusivo_semaforo}, f handle ficou = #{f}"  if false #2018Set26 - menos log

		t_inicio = Time.now
		segundos_decorridos = 0
		while not fez_lock_agora
			#2018Out6 - hash_param ganhou chaves :verbose e :segundos_sleep 
			t_agora = Time.now
			segundos_decorridos = t_agora - t_inicio
			segundos_sleep = hash_param[:segundos_sleep] || 1
			#2018Set30 - revamp, nao usar Timeout::timeout
			if f.flock(File::LOCK_NB|File::LOCK_EX)
				fez_lock_agora = true
				$already_locked[ctx] = true
			elsif segundos_decorridos > segundos_espera_flock
				$lock_exclusivo_falhou[ctx] = true  
				write_rsi_log :error, "executa_exclusivo:LOCK EXCLUSIVO nao obtido depois de #{segundos_espera_flock} segundos por , arquivo=#{nomearq_lock_exclusivo_semaforo}"
			end
			if hash_param[:verbose]
				write_rsi_log :debug, "executa_exclusivo: Esperando #{segundos_espera_flock} segundos por LOCK_EXCLUSIVO, segundos_decorridos = #{segundos_decorridos}, segundos_sleep=#{segundos_sleep}, arquivo=#{nomearq_lock_exclusivo_semaforo}" 
			end
			sleep segundos_sleep
		end
		if not $lock_exclusivo_falhou[ctx]
			if hash_param[:verbose]
				write_rsi_log :debug, "executa_exclusivo:SUCESSO em obter lock do arquivo de LOCK_EXCLUSIVO, arquivo=#{nomearq_lock_exclusivo_semaforo}, segundos_decorridos=#{segundos_decorridos}, segundos_espera_flock=#{segundos_espera_flock}" 
			end
		end
	end

	x=y=largura=altura=nil
	excecao_inicial = nil
	begin
		write_rsi_log :debug, "executa_exclusivo:iniciado trecho de execucao"  if false #2018Set26 - menos log
		if hash_param[:restore_janela_depois]  # RIBY: 
			write_rsi_log :trace, "executa_exclusivo:vai chamar get_rectangle_este_browser pra obter x,y,w,h "  if false #2018Set26 - menos log
			begin
				x,y,largura,altura = get_rectangle_este_browser
			rescue Exception => e
				write_rsi_log :warn, "executa_exclusivo: erro em get_rectangle_estr_browser, excecao=#{e}"
			end
			write_rsi_log :trace, "executa_exclusivo:chamou get_rectangle_este_browser e obteve x #{x} e y #{y} e altura #{altura} e largura #{largura} da janela antes de yield "  if false #2018Set26 - menos log
		end
		if hash_param[:maximizar_janela_antes]  # RIBY: 
			begin
				run_maximiza_este_browser 
				#2018Mar23 - se chegou parametro para maximizar, chama DIRETO o nucleo de run_maximixa_este_browser, a chamadora deve saber o que está fazendo... afinal de contas, estou muito provavelmente DENTRO DE LOCK EXCLUSIVO COM SUCESSO!
			rescue Exception => e
				write_rsi_log :warn, "executa_exclusivo: erro em maximima_este_browser, excecao=#{e}"
			end
			write_rsi_log :trace, "executa_exclusivo:maximizou janela antes de yield "  if false #2018Set26 - menos log
		end
	
		if hash_param[:trazer_janela_para_topo_antes]  # RIBY: 
			write_rsi_log :trace, "executa_exclusivo:vai trazer janela pro topo antes de yield "  if false #2018Set26 - menos log
			begin
				traz_esta_janela_pra_topo
			rescue Exception => e
				write_rsi_log :warn, "executa_exclusivo: erro em traz_esta_janela_pra_topo, excecao=#{e}"
			end
			write_rsi_log :trace, "executa_exclusivo:trouxe janela pro topo antes de yield "  if false #2018Set26 - menos log
		end
		dormir_yield=0
		write_rsi_log :trace, "executa_exclusivo:imediatamente antes de yield, dormirah #{dormir_yield}"  if false #2018Set26 - menos log
		sleep dormir_yield if dormir_yield > 0
		#begin; raise 'veja stack trace'; rescue Exception =>e; write_rsi_log :trace, e.backtrace; end
		ret = yield # SEM CONTROLE DE TIMEOUT na execucao do
		write_rsi_log :debug, "executa_exclusivo:imediatamente depois de yield, dormiu #{dormir_yield}"  if false #2018Set26 - menos log
		sleep dormir_yield if dormir_yield > 0
	rescue Exception => e
		write_rsi_log :error, "executa_exclusivo: excecao qye relancarei obtida nas proximidades de yield, excecao=#{e}, full exception backtrace=#{e.backtrace}"
		excecao_inicial = e
		raise e
	ensure
		excecao_no_ensure = nil
		begin
			write_rsi_log :debug, "executa_exclusivo:ensure"  if false #2018Set26 - menos log
			if hash_param[:restore_janela_depois]  # 2017Set9 - RESIZE ANTES DE UNLOCK!!! 
				write_rsi_log :trace, "executa_exclusivo:ensure, vai restaurar size de janela"  if false #2018Set26 - menos log
				
				begin
					if x
						#2017Out27 - fix "nao restaurava tamanho", passando x,y,largura,altura
						seta_rectangle_este_browser x,y,largura,altura  
					end
					reorganiza_janelas_topo				
				rescue Exception => e
					write_rsi_log :warn, "executa_exclusivo: erro em seta_rectangle_este_browser ou reorganiza_janelas_topo, excecao=#{e}" 
				end
				write_rsi_log :trace, "executa_exclusivo:ensure, restaurou size de janela e reorganizou topo"  if false #2018Set26 - menos log
			end
		rescue Exception => e
			write_rsi_log :trace, "executa_exclusivo:excecao em ensure de execua_exclusivo, excecao=#{e.inspect}"
			excecao_no_ensure = e
		end
		if fez_lock_agora #se nao entrou aqui já lockado, e tentou e conseguiu lockar
			write_rsi_log :trace, "executa_exclusivo:ensure, vai liberar lock - ( $already_locked[#{ctx}] = false + flock(File::LOCK_UN)"  if false #2018Set26 - menos log
			$already_locked[ctx] = false #2017Set10 am, faltava @$already_locked=false@ 
			f.flock(File::LOCK_UN)
			write_rsi_log :trace, "executa_exclusivo:ensure, liberou lock - ( $already_locked[#{ctx}] = false + flock(File::LOCK_UN)"  if false #2018Set26 - menos log
		end
		raise excecao_no_ensure if (excecao_no_ensure and not excecao_inicial)
	end

	write_rsi_log :debug, "executa_exclusivo:retornando, nao mostrando variavel ret aqui, retorno final, $already_locked[ctx] = #{$already_locked[ctx]}, $lock_exclusivo_falhou[ctx]=#{$lock_exclusivo_falhou[ctx]}"  if false #2018Set26 - menos log
	return ret 
end


def get_prefixo_ahk_processo
	return "ahk_TPN#{get_process_num}_"
end
def get_prefixo_ahk_geral
	return "ahk_TPN"
end

def del_exes_ahk_processo
	write_rsi_log :debug, "AHK running del todos do processo"
	c= "del features\\ahk\\#{get_prefixo_ahk_processo}*.exe"; write_rsi_log :debug, c; system c
end

def del_exes_ahk_geral
	write_rsi_log :debug, "AHK running del todos de todos processos"
	c= "del features\\ahk\\#{get_prefixo_ahk_geral}*.exe"; write_rsi_log :debug, c; system c
end

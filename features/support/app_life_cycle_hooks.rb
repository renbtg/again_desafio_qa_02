require_relative './methods_of_app_life_cycle_hooks.rb'

Before do |scenario|

	wrap_before_do scenario

end


After do |scenario|
#  if defined? $falhou_em_before_hook and $falhou_em_before_hook
 # 	next
 # end
  begin
 	wrap_after_do scenario
 rescue Exception => e
 	write_company_log :error, "After do |scenario| - EXCECAO INESPERADA=#{e}, FALHA DE IMPLEMENTACAO, em wrap_after_do"
 end
end


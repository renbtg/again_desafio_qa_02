require_relative './ipc_processo.rb'
def run_autohk_copia(exename, all_params_in_one_str="", segundos_timeout=get_segundos_fila_lock_exclusivo)
	#retval = nil
	#begin#2017Ago30, 20:30pm - protegendo contra TRAVAMENTO D
	#	Timeout::timeout(90, TimeoutDeRunAutohkCopia) do
	
	###########################
	# 2018mar8 - controle de tempo de exec_run_autohj_copia movido pra dentro do mesmo
	#
			retval =  exec_run_autohk_copia(exename, all_params_in_one_str, segundos_timeout)
	#
	#
	###########################

	#	end
	#rescue TimeoutDeRunAutohkCopia => e
	#	$LOG.error "run_autohk_copia: execucao nao concluida no tempo limite, TIMEOUT"
	#	raise "run_autohk_copia: execucao nao concluida no tempo limite, TIMEOUT"
	#end
	return retval
end
def exec_run_autohk_copia(exename, all_params_in_one_str="", limite_segundos = get_segundos_fila_lock_exclusivo)
	begin
		raise 'teste'
	rescue StandardError => e
		write_company_log "DEBUG exec_run_autohk_copia, full stack=#{e.backtrace}"
	end	

	tstart_geral=Time.now
	write_company_log :debug, "P00 - exec_run_autohk_copia, exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}" 
	if not (defined? $LOG and $LOG)
		write_company_log :debug,"" #workaround, console às vezes tem $LOG indefinido aqui!
		# este WORKAROUND apenas para se e quando eu voltar a usar $LOG aqui neste método
	end

	autdir_barrainv = get_automdir.gsub('/','\\')

	tstart_guid=Time.now
	rnd = obtem_guid #2017Set11-obtem GLOBAL UNIQUE ID: na prática, impossivel de duplicar
	tend_guid=Time.now
	segundos_guid=tend_guid.to_f-tstart_guid.to_f
	
	nname = "#{get_prefixo_ahk_processo}#{exename}_#{rnd}"
	$autohk_guid_file="#{autdir_barrainv}\\ahk_#{rnd}_outfile.out"

	tstart_copy=Time.now
	c= "copy #{autdir_barrainv}\\features\\ahk\\#{exename}.bmp #{autdir_barrainv}\\features\\ahk\\#{nname}.exe";  write_company_log :debug, c; system c
	tend_copy=Time.now
	segundos_copy=tend_copy.to_f-tstart_copy.to_f

	#write_company_log :debug, "AHK running copy"; 
	#c= "copy #{autdir_barrainv}\\features\\ahk\\#{exename}.bmp #{autdir_barrainv}\\features\\ahk\\bmp_#{nname}.bmp";  write_company_log :debug, c; system c

	#write_company_log :debug, "AHK running move"
	#c= "move #{autdir_barrainv}\\features\\ahk\\bmp_#{nname}.bmp #{autdir_barrainv}\\features\\ahk\\#{nname}.exe";  write_company_log :debug, c; system c

	c= "set TEST_AHK_OUTFILE=#{$autohk_guid_file} && #{autdir_barrainv}\\features\\ahk\\#{nname}.exe " + all_params_in_one_str 
	write_company_log :debug, c 
	pid=spawn(c)
	tstart_exec=Time.now
	write_company_log :debug, "exec_run_autohk_copia, exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}, fez spawn, pid=#{pid}, tstart_exec=#{tstart_exec}"
	
	timeoutada = false
	while processo_filho_executando? (pid)
		agora = Time.now
		elapsed = agora - tstart_exec
		write_company_log :debug, "exec_run_autohk_copia, exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}, executando, elapsed=#{elapsed}, limite_segundos=#{limite_segundos}, agora=#{agora}, tstart_exec=#{tstart_exec}"
		if elapsed > limite_segundos
			write_company_log :debug, "exec_run_autohk_copia, TIMEOUT ERROR, exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}, executando, elapsed=#{elapsed}, limite_segundos=#{limite_segundos}, agora=#{agora}, tstart_exec=#{tstart_exec}"
			timeoutada = true
			break
		end
		sleep 5
	end
	tend_exec=Time.now
	segundos_exec=tend_exec.to_f-tstart_exec.to_f
	exit_code=$?.exitstatus
    $autohk_exitstatus=exit_code #caso queriramos checar de forma parecida com $autohk_output: por GLOBAL
	write_company_log :debug, "AHK did run EXEC, timeoutada=#{timeoutada}, tend_exec=#{tend_exec}"

	limite_morte = 20
	tstart_morte = Time.now
	while processo_filho_executando? (pid)
		agora = Time.now
		elapsed = agora - limite_morte
		if elapsed > tempo_morte
			write_company_log :debug, "exec_run_autohk_copia, TIMEOUT CHECANDO MORTE ERROR, exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}, elapsed=#{elapsed}, limite_morte=#{limite_morte}, agora=#{agora}, tstart_morte=#{tstart_morte}"
			break
		end

		begin
			killret = Process.kill 9, pid
		rescue Errno::ESRCH => ersch 
			write_company_log :debug, "exec_run_autohk_copia, Errno:ERSCH (e=#{ersch}), tudo bem, em kill. Deve ter morrido entre checar pid NOHANG e kill!! exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}, após execucao, timeoutada=#{timeoutada}" 
		end
		write_company_log "exec_run_autohk_copia, killret=#{killret}"
		sleep 2
	end
	tend_morte=Time.now
	segundos_morte=tend_morte.to_f-tstart_morte.to_f
	write_company_log :debug, "AHK did matou, timeoutada=#{timeoutada}, tend_morte=#{tend_morte}, processo_filho_executando? (#{pid})=#{processo_filho_executando? (pid)}"

	segundos_del=0
	if false
		tstart_del=Time.now
		c= "del #{autdir_barrainv}\\features\\ahk\\#{nname}.exe"; write_company_log :debug, c; system c
		tend_del=Time.now
		segundos_del=tend_del.to_f-tstart_del.to_f
	end

	tend_geral=Time.now
	segundos_geral=tend_geral.to_f-tstart_geral.to_f

	# SLEEP DEPOIS de digitar e ENTER deve ser feito pela chamadora, em cada Page Object/STEP!

	write_company_log :debug, "AHK - exit_code=#{exit_code}"

	write_company_log :debug, "exec_run_autohk_copia, exename=#{exename}, all_params_in_one_str=#{all_params_in_one_str}, levou #{segundos_geral} pra geral, #{segundos_exec} para exec. #{segundos_copy} para copy, segundos_morte=#{segundos_morte}, #{segundos_del} para del,  #{segundos_guid} para guid, unexplained=#{segundos_geral - segundos_exec - segundos_copy - segundos_morte - segundos_del - segundos_guid}"
	return exit_code
end
